package org.manathome.props2consts;

import static org.assertj.core.api.Assertions.assertThat;  

import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.props2consts.Props2ConstsTask;
 
/**
 * A simple unit test for plugin.
 */
@Tag("plugin")
public class Props2ConstsPluginTest {
	
    @Test 
    public void pluginRegistersItsTask() {
        assertThat(
        		getTaskForTest())
        .isNotNull();
    }
    	
    @Test 
    public void pluginTaskHasOutputsProperties() {
        assertThat(getTaskForTest().getOutputs().getHasOutput()).isTrue();
    }    

    private Task getTaskForTest() {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply("org.manathome.props2consts");
        return project.getTasks().findByName(Props2ConstsTask.PROPERTIESCONSTS_TASK_NAME);
    }
}
