package org.manathome.props2consts;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Properties;

import org.junit.jupiter.api.Test;

public class ConstantGeneratorTest {
	
	final ConstantGenerator generator = new ConstantGenerator();

	@Test 
	public void testLoad() {
		Properties props = generator.loadFromResource("/samples/messages.properties");
		assertThat(props).isNotNull(); 
		assertThat(props).containsKey("project.name.required");
	}
	
	@Test 
	public void testInvalidLoad() {
		assertThrows(IllegalArgumentException.class, () -> {
			generator.loadFromResource("NOT_KNOWN/messages.properties");
		});
	}	
	
	@Test 
	public void testGenerate() {
		Properties props = generator.loadFromResource("/samples/messages.properties");
		final String src = generator.generate(props , "org.manathome.test", "LocalizedMessages", "from messages.properties");
		
		System.out.println(src);
		assertThat(src).isNotBlank();
		assertThat(src).contains("LocalizedMessages");
	}
	
	@Test 
	public void testGenerateKey() {
		Properties props = generator.loadFromResource("/samples/messages.properties");
		final String src = generator.generate(props, "org.manathome.test", "LocalizedMessages", "");
		assertThat(src).containsIgnoringCase("Name");
	}	
	
}
