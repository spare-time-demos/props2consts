package org.manathome.props2consts;

import static org.assertj.core.api.Assertions.assertThat;  

import org.junit.jupiter.api.Test;
import org.manathome.props2consts.generated.LocalizedMessages;

/** not a unit test, 
 * but can be run after call of  gradlew -b build.pluginrun.gradle generateMessageConstants
 * as it imports the generated source file and access its constants.  
 * @author manfred
 */
public class GeneratedMessagesTest {

	@Test
	public void testMessagesGenerates() {
		assertThat(LocalizedMessages.project_name_required).isEqualTo("project.name.required");
	}
}
