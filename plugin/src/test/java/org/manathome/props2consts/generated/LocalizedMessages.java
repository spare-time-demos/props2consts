package org.manathome.props2consts.generated;

import javax.annotation.processing.Generated;


/** generated property key constants.
  *
  *  @author Props2ConstsPlugin
  */
@Generated(comments = "from messages.properties",
              value = "org.manathome.props2consts.Props2ConstsPlugin")
public interface LocalizedMessages {

  /** property key project.lead.not.you.
   *{@code PROJ3: you are not a lead of this project. }
   */
  static final String project_lead_not_you = "project.lead.not.you";

  /** property key user.name.required.
   * 
   *{@code USRN1: user name is required. }
   */
  static final String user_name_required = "user.name.required";

  /** property key user.id.required.
   *{@code USRN2: user id is required. }
   */
  static final String user_id_required = "user.id.required";

  /** property key project.name.required.
   *{@code PROJ1: user name is required. }
   */
  static final String project_name_required = "project.name.required";

  /** property key task.name.required.
   *{@code TASK1: task name is required. }
   */
  static final String task_name_required = "task.name.required";

  /** property key login.success.message.
   *{@code LOK: login successful as user: {0}. }
   */
  static final String login_success_message = "login.success.message";

  /** property key login.failed.message.
   *{@code LERR1: invalid user or password, login into time@work failed. }
   */
  static final String login_failed_message = "login.failed.message";

  /** property key project.lead.not.remove.yourself.
   *{@code PROJ2: you can not remove yourself as lead. }
   */
  static final String project_lead_not_remove_yourself = "project.lead.not.remove.yourself";

}