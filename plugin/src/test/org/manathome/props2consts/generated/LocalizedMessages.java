
/** generated property key constants. */
public interface LocalizedMessages.java {

   /** property: PROJ3: you are not a lead of this project.. */
   public final String project_lead_not_you = "project.lead.not.you";

   /** property: USRN1: user name is required.. */
   public final String user_name_required = "user.name.required";

   /** property: USRN2: user id is required.. */
   public final String user_id_required = "user.id.required";

   /** property: PROJ1: user name is required.. */
   public final String project_name_required = "project.name.required";

   /** property: TASK1: task name is required.. */
   public final String task_name_required = "task.name.required";

   /** property: LOK: login successful as user: {0}.. */
   public final String login_success_message = "login.success.message";

   /** property: LERR1: invalid user or password, login into time@work failed.. */
   public final String login_failed_message = "login.failed.message";

   /** property: PROJ2: you can not remove yourself as lead.. */
   public final String project_lead_not_remove_yourself = "project.lead.not.remove.yourself";

}
