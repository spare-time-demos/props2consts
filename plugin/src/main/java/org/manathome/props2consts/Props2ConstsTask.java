package org.manathome.props2consts;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

/** gradle build task. */
public class Props2ConstsTask extends DefaultTask {

	public static final String PROPERTIESCONSTS_TASK_NAME = "propertyConstantsGenerate";	

	private File constantsFile = null;
	
	private File propertiesFile = null;
	
	private String packageName = null;
		
	@OutputFile
	public File getConstantsFile() {
		return constantsFile;
	}

	/** generated constant java source file with constants from keys. 
	 * 
	 * @param constantsFile to be generated source file
	 * */
	public void setConstantsFile(final File constantsFile) {
		this.constantsFile = constantsFile;
	}

	@InputFile
	public File getPropertiesFile() {
		return propertiesFile;
	}

	/** input xy.properties file with keys. 
	 * 
	 * @param propertiesFile existing properties file to read.
	 */
	public void setPropertiesFile(final File propertiesFile) {
		this.propertiesFile = propertiesFile;
	}
	

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/** ctor. */
	public Props2ConstsTask() {
		super();
		setGroup("generate");
		setDescription("parse properties file and generate java source files from its keys.");

		this.getLogger().debug("ctor " + PROPERTIESCONSTS_TASK_NAME);
	}

	/** read and generate.
	 * @throws Exception on read/write error on properties or source files.  
	 */
	@TaskAction
	public void props2constsGenerate() throws Exception {
		
		this.getLogger().info("reading properties ....: " + this.getPropertiesFile().getCanonicalFile());		
		this.getLogger().info("generating constants ..: " + this.getConstantsFile().getCanonicalFile());
		
		if(this.getPropertiesFile() == null 
				||  this.getPropertiesFile() == null 
				|| !this.getPropertiesFile().canRead() 
				|| !this.getPropertiesFile().isFile() 
				|| ! this.getPropertiesFile().exists()) {
			throw new RuntimeException("please give existing PropertiesFile, not " + this.getPropertiesFile());
		}
		
		if(this.getConstantsFile() == null) {
			throw new RuntimeException("please give an constant file to write, not " + this.getConstantsFile());			
		}
		
		ConstantGenerator cg = new ConstantGenerator();
		Properties props = cg.load(this.getPropertiesFile().getCanonicalPath());
		
		String src = cg.generate(
				props, 
				this.packageName, 
				this.getConstantsFile().getName().replace(".java", ""),
				"with keys from " + this.getPropertiesFile().getName()
				);
		
		cg.save(src, new FileOutputStream(this.getConstantsFile()));
		
	}


}
