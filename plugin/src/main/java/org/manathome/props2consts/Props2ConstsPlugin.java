package org.manathome.props2consts;

import org.gradle.api.Project;
import org.gradle.api.Plugin;

/**
 * gradle plugin: generate java constants
 */
public class Props2ConstsPlugin implements Plugin<Project> {
	
	@Override
    public void apply(Project project) {
    	
		project.getLogger().debug("apply jira2doc plugin.");
					    
	    project.getTasks().register(
	    		Props2ConstsTask.PROPERTIESCONSTS_TASK_NAME,
	    		Props2ConstsTask.class);
    }
    
}
