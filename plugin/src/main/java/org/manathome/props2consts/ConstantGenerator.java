package org.manathome.props2consts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ConstantGenerator {

	public Properties loadFromResource(final String resourceName) {
		InputStream in = getClass().getResourceAsStream(resourceName);
		if(in == null) {
			throw new IllegalArgumentException("not a valid resource to load: " + resourceName);
		}
		return  load(in);
	}	

	public Properties load(final String fileName) {
		try {
			return load(new FileInputStream(fileName));
		} catch( FileNotFoundException fnf) {
            throw new RuntimeException("error reading properties: file " + fileName + ": " + fnf.getLocalizedMessage(), fnf);			
		}
	}
	
	public Properties load(final InputStream in) {

		if(in == null) {
			throw new NullPointerException("no properties input stream, was null");
		}
		
		try (InputStream input = in) {

            Properties prop = new Properties();
            prop.load(input);
            return prop;
        } catch (IOException ex) {
            throw new RuntimeException("error reading properties: " + ex.getLocalizedMessage(), ex);
        }		
	}	
	

	/** generate java constant source file from property keys.
	 * 
	 * @param properties properties to generate constants for
	 * @param packageName package where constant interface should be generated
	 * @param constInterfaceName interface name containing the constants
	 * @param comment will go into generated annotations comment section 
	 * @return java source
	 * */
	public String generate(
			final Properties properties, 
			final String packageName, 
			final String constInterfaceName,
			final String comment) {
		
		if(properties == null) {
			throw new NullPointerException("no properties, was null");
		}
		
		final StringBuffer source =  new StringBuffer("");
		
		source.append("package " + packageName + ";\n\n");		
		source.append("import javax.annotation.processing.Generated;\n\n\n");
		
		source.append("/** generated property key constants.\n");
		source.append("  *\n");
		source.append("  *  @author Props2ConstsPlugin\n");
		source.append("  */\n");
		source.append("@Generated(comments = \""+ comment + "\",\n");
		source.append("              value = \"" + Props2ConstsPlugin.class.getName() + "\")\n");
		source.append("public interface " + constInterfaceName + " {\n\n");
		
		properties.forEach((key, val) -> {
			String escapedVal = val == null ? "" : val.toString().replace("*/", "*_/");
			source.append("  /** property key " + key + ".\n");
			source.append("   *{@code " + escapedVal + " }\n" );
			source.append("   */\n");
			source.append("  static final String " + key.toString().replace(".", "_") + " = \"" + key + "\";\n\n");
		});
		
		source.append("}\n");
		
		return source.toString();
	}
	
	
	public void save(String text, OutputStream out) throws IOException {
		try(Writer writer = new PrintWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8), true)) {
			writer.append(text);
			writer.flush();
		}
	}
}
